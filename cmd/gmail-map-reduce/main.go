// Most of this is shamelessly stolen from the gmail api quickstart guide
package main

import (
	"bufio"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/gmail/v1"
	"google.golang.org/api/option"
)

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.Background(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func main() {
	fmt.Printf("gmail Query: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	query := scanner.Text()
	fmt.Println()

	ctx := context.Background()
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, gmail.GmailReadonlyScope)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	client := getClient(config)

	srv, err := gmail.NewService(ctx, option.WithHTTPClient(client))
	if err != nil {
		log.Fatalf("Unable to retrieve Gmail client: %v", err)
	}

	if err := srv.Users.Threads.List("me").Q(query).Pages(ctx, func(r *gmail.ListThreadsResponse) error {
		for _, t := range r.Threads {
			thread, err := srv.Users.Threads.Get("me", t.Id).Format("full").Do()
			if err != nil {
				return err
			}

			for _, m := range thread.Messages {
				var subject, from string

				for _, h := range m.Payload.Headers {
					if h.Name == "Subject" {
						subject = h.Value
					} else if h.Name == "From" {
						from = h.Value
					}
				}

				body, err := bodyFromPart(m.Payload)
				if err != nil {
					return err
				}

				fmt.Printf("%s %s %s: %q\n", subject, time.Unix(m.InternalDate/1000, 0).Format(time.RFC3339), from, body)

			}
		}
		return nil
	}); err != nil {
		log.Fatal(err)
	}
}

func bodyFromPart(part *gmail.MessagePart) (string, error) {
	if part == nil {
		return "", nil
	}

	var body string
	switch part.MimeType {
	case "image/jpeg", "image/png":
	case "text/html":
		body = "<html that didn't render for me>"
	case "text/plain":
		bs, _ := base64.StdEncoding.DecodeString(part.Body.Data)
		// ignoring that it may fail sometimes
		body = string(bs)
	case "multipart/mixed", "multipart/alternative":
		parts := make([]string, 0)
		for _, p := range part.Parts {
			newP, err := bodyFromPart(p)
			if err != nil {
				return "", err
			}
			parts = append(parts, newP)
			body = strings.Join(parts, "\n")
		}
	default:
		return "", fmt.Errorf("Unknown mimetype: %q", part.MimeType)
	}

	return body, nil
}
