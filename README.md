gmail-map-reduce
================

This application will:
1. search gmail for your particular query (taken via stdin at a prompt)
2. Print out text-based representations of every message in that query, in the format:

```
<subject> <RFC3339> <from>: "<message>"
```

This format should prove to be easily sortable with coreutils sort

An example query would be `subject: SMS from`
